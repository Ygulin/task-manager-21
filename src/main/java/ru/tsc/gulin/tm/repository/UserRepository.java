package ru.tsc.gulin.tm.repository;

import ru.tsc.gulin.tm.api.repository.IUserRepository;
import ru.tsc.gulin.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findOneByLogin(final String login) {
        return models.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findOneByEmail(final String email) {
        return models.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findOneByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

}
