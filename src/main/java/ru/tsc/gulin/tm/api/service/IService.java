package ru.tsc.gulin.tm.api.service;

import ru.tsc.gulin.tm.api.repository.IRepository;
import ru.tsc.gulin.tm.enumerated.Sort;
import ru.tsc.gulin.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    List<M> findAll(Sort sort);

}
