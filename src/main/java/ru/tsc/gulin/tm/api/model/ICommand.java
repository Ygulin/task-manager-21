package ru.tsc.gulin.tm.api.model;

public interface ICommand {

    String getName();

    String getDescription();

    String getArgument();

}
