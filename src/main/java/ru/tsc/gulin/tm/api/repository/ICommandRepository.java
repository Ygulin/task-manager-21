package ru.tsc.gulin.tm.api.repository;

import ru.tsc.gulin.tm.command.AbstractCommand;

import java.util.Collection;


public interface ICommandRepository {

    Collection<AbstractCommand> getTerminalCommands();

    void add(AbstractCommand command);

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String argument);

}
