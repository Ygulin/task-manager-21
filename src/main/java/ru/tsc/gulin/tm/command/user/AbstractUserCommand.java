package ru.tsc.gulin.tm.command.user;

import ru.tsc.gulin.tm.api.service.IUserService;
import ru.tsc.gulin.tm.command.AbstractCommand;
import ru.tsc.gulin.tm.enumerated.Role;
import ru.tsc.gulin.tm.exception.entity.UserNotFoundException;
import ru.tsc.gulin.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
